package Main;

import QAP.AlgoritmosGeneticos.AGEstacionario;
import QAP.AlgoritmosGeneticos.AGGeneracional;
import QAP.AlgoritmosGeneticos.AlgoritmoGenetico;
import QAP.Greedy;
import QAP.ModelSolucion;
import QAP.SolucionQAP;
import Util.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public class Practica2 {
    public static void main(String [] args){

        Print printer = new PrintToConsole();

        printer.printCabecera();

        int tipoSolucion = printer.menuTipoSolucion();
        int tipoCruce = 0;
        if(tipoSolucion != Constantes.GREEDY){
            tipoCruce = printer.menuTipoCruce()-1;
        }
        int problemIndex = printer.menuProblemas()-1;
        int numIteraciones = 1;
        if(problemIndex !=21 && tipoSolucion < 3) numIteraciones = printer.menuEjecuciones();
        long semilla = 0;
        if(tipoSolucion != Constantes.GREEDY){
            semilla = printer.menuSemilla();
        }
        //715225739

        String path;
        AlgoritmoGenetico agg;
        AlgoritmoGenetico age;
        SolucionQAP greedy;

        try{
            switch (tipoSolucion){
                case Constantes.AGG:
                    if(problemIndex != Constantes.PROBLEMAS.length){

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        agg = new AGGeneracional(path, semilla, tipoCruce);

                        ArrayList<ModelSolucion> solucionAGG = agg.getNsoluciones(numIteraciones);
                        printer.printResultados(solucionAGG, tipoSolucion, problemIndex);

                    } else {
                        for(int i = 0; i < Constantes.PROBLEMAS.length; i++){

                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            agg = new AGGeneracional(path, semilla, tipoCruce);

                            ArrayList<ModelSolucion> solucionAGG = agg.getNsoluciones(numIteraciones);
                            printer.printResultados(solucionAGG, tipoSolucion, i);
                        }
                    }
                    break;
                case Constantes.AGE:
                    if(problemIndex != Constantes.PROBLEMAS.length){

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        age = new AGEstacionario(path, semilla, tipoCruce);

                        ArrayList<ModelSolucion> solucionAGE = age.getNsoluciones(numIteraciones);
                        printer.printResultados(solucionAGE, tipoSolucion, problemIndex);

                    } else {
                        for(int i = 0; i < Constantes.PROBLEMAS.length; i++){

                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            age = new AGEstacionario(path, semilla, tipoCruce);

                            ArrayList<ModelSolucion> solucionAGE = age.getNsoluciones(numIteraciones);
                            printer.printResultados(solucionAGE, tipoSolucion, i);
                        }
                    }
                    break;
                case Constantes.GREEDY:
                    if(problemIndex != Constantes.PROBLEMAS.length){

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        greedy = new Greedy(path);

                        ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);
                        printer.printResultados(solucionGreedy, tipoSolucion, problemIndex);

                    } else {
                        for(int i = 0; i < Constantes.PROBLEMAS.length; i++){

                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            greedy = new Greedy(path);

                            ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);
                            printer.printResultados(solucionGreedy, tipoSolucion, i);
                        }
                    }
                    break;
                case 4:
                    if(problemIndex != Constantes.PROBLEMAS.length){

                        path = "data/" + Constantes.PROBLEMAS[problemIndex].toLowerCase() + ".dat";
                        agg = new AGGeneracional(path, semilla, tipoCruce);
                        age = new AGEstacionario(path, semilla, tipoCruce);
                        greedy = new Greedy(path);

                        ArrayList<ModelSolucion> solucionAGE = age.getNsoluciones(numIteraciones);
                        ArrayList<ModelSolucion> solucionAGG = agg.getNsoluciones(numIteraciones);
                        ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);

                        printer.printResultados(solucionAGG, Constantes.AGG, problemIndex);
                        printer.printResultados(solucionAGE, Constantes.AGE, problemIndex);
                        printer.printResultados(solucionGreedy, Constantes.GREEDY, problemIndex);

                    } else {
                        for(int i = 0; i < Constantes.PROBLEMAS.length; i++){
                            path = "data/" + Constantes.PROBLEMAS[i].toLowerCase() + ".dat";
                            agg = new AGGeneracional(path, semilla, tipoCruce);
                            age = new AGEstacionario(path, semilla, tipoCruce);
                            greedy = new Greedy(path);

                            ArrayList<ModelSolucion> solucionAGG = agg.getNsoluciones(numIteraciones);
                            ArrayList<ModelSolucion> solucionAGE = age.getNsoluciones(numIteraciones);
                            ArrayList<ModelSolucion> solucionGreedy = greedy.getNsoluciones(numIteraciones);

                            printer.printResultados(solucionAGG, Constantes.AGG, i);
                            printer.printResultados(solucionAGE, Constantes.AGE, i);
                            printer.printResultados(solucionGreedy, Constantes.GREEDY, i);
                        }
                    }
                    break;
            }

        }catch (FileNotFoundException e) {
            printer.mostrarFileNotFoundError(e);
            e.printStackTrace();
        }

    }
}
