package Util;

import QAP.ModelSolucion;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import static Main.Constantes.*;

public class PrintToConsole implements Print {

    public void printResultados(ArrayList<ModelSolucion> soluciones, int tipoSolucion, int indexProblem) {
        String enfoque = getEnfoque(tipoSolucion);
        if(soluciones.size() == 1) {
            System.out.println("Solución " + enfoque + " para el problema " + PROBLEMAS[indexProblem]);
            System.out.println("---------------------------------------");
            System.out.println(soluciones.get(0));
        }else {
            for (int i = 0; i < soluciones.size(); i++){
                System.out.println("Ejecución " + (i+1) + " " + enfoque + " " + PROBLEMAS[indexProblem]);
                System.out.println(soluciones.get(i));
            }

            printEstadisticas(soluciones, tipoSolucion, indexProblem);
        }
    }

    private void printEstadisticas(ArrayList<ModelSolucion> soluciones, int tipo, int indexProblem){
        ModelSolucion mejor = soluciones.get(0);
        ModelSolucion peor = mejor;

        for (ModelSolucion solucion : soluciones) {
            if (solucion.getCoste() > peor.getCoste())
                peor = solucion;
            else if (solucion.getCoste() < mejor.getCoste())
                mejor = solucion;
        }

        System.out.println("Estadísticas del problema " + PROBLEMAS[indexProblem] + " usando el enfoque " + getEnfoque(tipo));
        System.out.println("---------------------------------------------------------");

        System.out.println("Coste de la mejor solución: " + mejor.getCoste());
        System.out.println("Coste de la peor solución: " + peor.getCoste());

        double desviacion = Estadisticas.desviacionMedia(soluciones, mejor);
        double tiempoMedio = Estadisticas.tiempoMedio(soluciones);
        System.out.println("Desviación media: " + desviacion);
        System.out.println("Tiempo medio: " + tiempoMedio);
        System.out.println();
    }

    private String getEnfoque(int tipo){
        String enfoque;

        switch (tipo){
            case AGG:
                enfoque = "AGG";
                break;
            case AGE:
                enfoque = "AGE";
                break;
            case GREEDY:
                enfoque = "Greedy";
                break;
            default:
                enfoque = "";
        }

        return enfoque;
    }

    public int menuTipoSolucion(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el método a usar para resolver el problema:");
            System.out.println("\t" + AGG + "- Algoritmo Genético Generacional");
            System.out.println("\t"+ AGE +"- Algoritmo Genético Estacionario");
            System.out.println("\t"+ GREEDY +"- Algoritmo Greedy");
            System.out.println("\t"+ "4- Comparativa con todos los métodos");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 4);

        return tipo;
    }

    public int menuTipoCruce(){
        Scanner in = new Scanner(System.in);
        int tipo;
        do {
            System.out.println("Escoge el cruce a usar para resolver el problema:");
            System.out.println("\t" + CrucePosicion + "- Cruce posición");
            System.out.println("\t"+ CrucePMX +"- Cruce PMX");
            System.out.println("\t"+ CruceOX +"- Cruce OX");

            tipo = in.nextInt();
        }while(tipo < 1 || tipo > 3);

        return tipo;
    }

    public int menuProblemas(){
        Scanner in = new Scanner(System.in);
        int problemIndex;
        do{
            System.out.println("Escoge el problema que quieres resolver:");
            for(int i = 0; i < PROBLEMAS.length; i++){
                System.out.println("\t" + (i+1) + "- " + PROBLEMAS[i]);
            }
            System.out.println("\t" + (PROBLEMAS.length+1) + "- " + "Todos los problemas");
            problemIndex = in.nextInt();

        }while(problemIndex > (PROBLEMAS.length + 1) || problemIndex < 1);

        return problemIndex;
    }

    public int menuEjecuciones(){
        Scanner in = new Scanner(System.in);
        int numIteration;
        System.out.println("Escoge el número de ejecuciones a realizar:");
        numIteration = in.nextInt();
        return numIteration;
    }

    public long menuSemilla(){
        Scanner in = new Scanner(System.in);
        long semilla;
        System.out.println("Escoge la semilla para la solución inicial:");
        semilla = in.nextLong();

        return semilla;
    }

    @Override
    public void mostrarFileNotFoundError(FileNotFoundException exc) {
        System.out.println("La ruta especificada para el archivo no existe: " + exc.getMessage());
    }

    @Override
    public void printCabecera() {
        System.out.println("Práctica 2. Metaheurísticas");
        System.out.println();
    }
}
