package Util;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by mpancorbo on 24/10/14.
 */
public class QAPReader {

    private static final int ORDEN_FLUJOS_DISTANCIAS = 0;
    private static final int ORDEN_DISTANCIAS_FLUJOS = 1;
    
    private int tam;
    private int[][] flujos;
    private int[][] distancias;
    
    public QAPReader(String path) throws FileNotFoundException{
        Scanner scanner = new Scanner(new File(path));
        this.tam = scanner.nextInt();
        int orden = scanner.nextInt();
        if(orden == ORDEN_DISTANCIAS_FLUJOS){
            setMatrizDistancias(scanner);
            setMatrizFlujos(scanner);
        }else if(orden == ORDEN_FLUJOS_DISTANCIAS){
            setMatrizFlujos(scanner);
            setMatrizDistancias(scanner);
        }
    }

    private void setMatrizFlujos(Scanner scanner) {
        this.flujos= new int[this.tam][this.tam];
        for(int i = 0; i < this.tam; i++)
            for(int j = 0; j < this.tam; j++)
                this.flujos[i][j] = scanner.nextInt();
    }

    private void setMatrizDistancias(Scanner scanner) {
        this.distancias = new int[this.tam][this.tam];

        for(int i = 0; i < this.tam; i++)
            for(int j = 0; j < this.tam; j++)
                this.distancias[i][j] = scanner.nextInt();
    }

    public int getTam() {
        return tam;
    }

    public int[][] getFlujos() {
        return flujos;
    }

    public int[][] getDistancias() {
        return distancias;
    }
}
