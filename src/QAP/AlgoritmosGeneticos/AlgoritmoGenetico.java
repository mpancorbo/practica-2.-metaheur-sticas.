package QAP.AlgoritmosGeneticos;

import Main.Constantes;
import QAP.*;
import Util.SortedArrayList;
import java.io.FileNotFoundException;

public abstract class AlgoritmoGenetico extends SolucionQAP {

    protected static final int TAM_POBLACION = 50;

    protected int usosFuncionObjetivo;

    protected AlgoritmoGenetico(String path, long semilla) throws FileNotFoundException {
        super(path, semilla);
    }

    protected abstract SortedArrayList<Cromosoma> operadorSeleccion(SortedArrayList<Cromosoma> poblacionInicial);
    protected abstract SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion);
    protected abstract SortedArrayList<Cromosoma> reemplazamiento(SortedArrayList<Cromosoma> poblacionInicial, SortedArrayList<Cromosoma> poblacionMutada);

    public ModelSolucion getSolucion(){
        long startTime = System.currentTimeMillis();
        SortedArrayList<Cromosoma> poblacion = this.generaPoblacionInicial();
        int usosFuncionObjetivo = 0;

        while (usosFuncionObjetivo < 20000){
            Generacion generacion = this.siguienteGeneracion(poblacion);
            poblacion = generacion.getPoblacion();
            usosFuncionObjetivo += generacion.getUsosFuncionObjetivo();
        }

        return new ModelSolucion(System.currentTimeMillis() - startTime, poblacion.get(0));
    }

    public Generacion siguienteGeneracion(SortedArrayList<Cromosoma> poblacionInicial){
        this.usosFuncionObjetivo = 0;

        SortedArrayList<Cromosoma> nuevaPoblacion = operadorSeleccion(poblacionInicial);
        nuevaPoblacion = operadorCruce(nuevaPoblacion);
        nuevaPoblacion = operadorMutacion(nuevaPoblacion);
        nuevaPoblacion = reemplazamiento(poblacionInicial, nuevaPoblacion);

        return new Generacion(nuevaPoblacion, this.usosFuncionObjetivo);
    }

    public SortedArrayList<Cromosoma> generaPoblacionInicial(){
        SortedArrayList<Cromosoma> poblacion = new SortedArrayList<Cromosoma>();

        for(int i = 0; i < TAM_POBLACION; i++) {
            int[] permutacionCromosoma = this.generaSolucionAleatoria();
            int costeCromosoma = this.funcionObjetivo(permutacionCromosoma);
            poblacion.add(new Cromosoma(permutacionCromosoma, costeCromosoma));
        }

        return poblacion;
    }

    protected SortedArrayList<Cromosoma> operadorMutacion(SortedArrayList<Cromosoma> poblacion){

        int numeroMutaciones = (int) (poblacion.size() * this.tamProblema * 0.01);

        for(int i = 0; i < numeroMutaciones; i++){
            int randomIndex = this.random.nextInt(poblacion.size());
            int[] randomCromosoma = poblacion.get(randomIndex).getPermutacion().clone();

            int indexA = this.random.nextInt(this.tamProblema);
            int indexB;

            do{
                indexB = this.random.nextInt(this.tamProblema);
            }while(indexB == indexA);

            int swapAux = randomCromosoma[indexA];
            randomCromosoma[indexA] = randomCromosoma[indexB];
            randomCromosoma[indexB] = swapAux;

            poblacion.set(randomIndex, new Cromosoma(randomCromosoma, funcionObjetivo(randomCromosoma)));
        }

        return poblacion;
    }

    protected Cromosoma torneoBinario(SortedArrayList<Cromosoma> poblacionInicial){

        Cromosoma padre1, padre2;
        int tamPoblacion = poblacionInicial.size();

        padre1 = poblacionInicial.get(this.random.nextInt(tamPoblacion));
        padre2 = poblacionInicial.get(this.random.nextInt(tamPoblacion));

        if(padre1.compareTo(padre2) < 0)
            return padre1;

        return padre2;
    }

    protected OperadorCruceInterface getOperadorCruce(int tipoOperadorCruce){
        OperadorCruceInterface operadorCruce;
        switch (tipoOperadorCruce){
            case Constantes.CrucePosicion:
                operadorCruce = new OperadorCrucePosicion(this.tamProblema, this.flujos, this.distancias, this.random);
                break;
            case Constantes.CrucePMX:
                operadorCruce = new OperadorCrucePMX(this.tamProblema, this.flujos, this.distancias, this.random);
                break;
            case Constantes.CruceOX:
                operadorCruce = new OperadorCruceOX(this.tamProblema, this.flujos, this.distancias, this.random);
                break;
            default:
                throw new IllegalArgumentException();
        }

        return operadorCruce;
    }

    @Override
    protected int funcionObjetivo(int[] solucion) {
        usosFuncionObjetivo++;

        return super.funcionObjetivo(solucion);
    }
}
