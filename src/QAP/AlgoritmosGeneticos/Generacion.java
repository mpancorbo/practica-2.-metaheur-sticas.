package QAP.AlgoritmosGeneticos;

import Util.SortedArrayList;

public class Generacion {

    private SortedArrayList<Cromosoma> poblacion;
    private int usosFuncionObjetivo;

    public Generacion(SortedArrayList<Cromosoma> poblacion, int usosFuncionObjetivo){
        this.poblacion = poblacion;
        this.usosFuncionObjetivo = usosFuncionObjetivo;
    }

    public SortedArrayList<Cromosoma> getPoblacion() {
        return poblacion;
    }

    public int getUsosFuncionObjetivo() {
        return usosFuncionObjetivo;
    }
}
