package QAP.AlgoritmosGeneticos;

import Util.SortedArrayList;

import java.io.FileNotFoundException;

public class AGEstacionario extends AlgoritmoGenetico {

    protected static final double PROBABILIDAD_CRUCE = 1;
    protected static final int NUMERO_CRUCES = (int) (1 * AGEstacionario.PROBABILIDAD_CRUCE);

    private OperadorCruceInterface operadorCruce;

    public AGEstacionario(String path, long semilla, int tipoOperadorCruce) throws FileNotFoundException {
        super(path, semilla);
        this.operadorCruce = getOperadorCruce(tipoOperadorCruce);
    }

    @Override
    protected SortedArrayList<Cromosoma> operadorSeleccion(SortedArrayList<Cromosoma> poblacionInicial) {
        SortedArrayList<Cromosoma> padresSeleccionados = new SortedArrayList<Cromosoma>();

        for(int i = 0; i < 2; i++)
            padresSeleccionados.add(torneoBinario(poblacionInicial));

        return padresSeleccionados;
    }

    @Override
    protected  SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion){
        this.usosFuncionObjetivo += AGEstacionario.NUMERO_CRUCES*2;
        return operadorCruce.operadorCruce(poblacion, AGEstacionario.NUMERO_CRUCES);
    }

    @Override
    protected SortedArrayList<Cromosoma> reemplazamiento(SortedArrayList<Cromosoma> poblacionInicial, SortedArrayList<Cromosoma> poblacionMutada) {

        Cromosoma peor = poblacionInicial.get(0);
        Cromosoma segundoPeor = poblacionInicial.get(1);

        Cromosoma mejorMutado = poblacionMutada.get(1);
        Cromosoma peorMutado = poblacionMutada.get(0);

        if(mejorMutado.compareTo(peor) < 0){
            if((mejorMutado.compareTo(segundoPeor) < 0) && (peorMutado.compareTo(peor) < 0)){
                poblacionInicial.set(1, mejorMutado);
                poblacionInicial.set(0, peorMutado);
            }else{
                poblacionInicial.set(0, mejorMutado);
            }
        }

        return poblacionInicial;
    }
}
