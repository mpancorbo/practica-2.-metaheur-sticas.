package QAP.AlgoritmosGeneticos;

import QAP.QAP;
import Util.SortedArrayList;
import java.util.Random;

public class OperadorCrucePosicion extends QAP implements OperadorCruceInterface {
    public OperadorCrucePosicion(int tam, int[][] flujos, int[][] distancias, Random random) {
        super(tam, flujos, distancias, random);
    }

    @Override
    public SortedArrayList<Cromosoma> operadorCruce(SortedArrayList<Cromosoma> poblacion, int numeroCruces) {
        SortedArrayList<Cromosoma> hijos;

        for(int i = 0; i < numeroCruces; i++){
            int indexPadreA = this.random.nextInt(poblacion.size());
            int indexPadreB = this.random.nextInt(poblacion.size());

            hijos = this.operadorCrucePosicion(poblacion.get(indexPadreA), poblacion.get(indexPadreB));

            poblacion.set(indexPadreA, hijos.get(0));
            poblacion.set(indexPadreB, hijos.get(1));
        }
        return poblacion;
    }

    public SortedArrayList<Cromosoma> operadorCrucePosicion(Cromosoma padre1, Cromosoma padre2) {

        int[] hijoBase = new int[this.tamProblema];
        int numAlelosDistintos = 0;
        int[] permutacionPadre1 = padre1.getPermutacion();
        int[] permutacionPadre2 = padre2.getPermutacion();

        SortedArrayList<Cromosoma> hijos = new SortedArrayList<Cromosoma>();

        for(int i = 0; i < hijoBase.length; i++){
            if(permutacionPadre1[i] == permutacionPadre2[i]) {
                hijoBase[i] = permutacionPadre1[i];
            }else {
                hijoBase[i] = -1;
                numAlelosDistintos++;
            }
        }

        int[] alelosDistintos = new int[numAlelosDistintos];
        int alelosDistintosIndex = 0;

        for(int i = 0; i < hijoBase.length; i++) {
            if (hijoBase[i] == -1) {
                alelosDistintos[alelosDistintosIndex] = permutacionPadre1[i];
                alelosDistintosIndex++;
            }
        }

        int[] permutacion;

        for(int i = 0; i < 2; i++){
            permutacion = generaPermutacionAleatoria(alelosDistintos);
            permutacion = getHijo(hijoBase, permutacion);
            hijos.add(new Cromosoma(permutacion, this.funcionObjetivo(permutacion)));
        }

        return hijos;
    }

    private int[] getHijo(int[] hijoBase, int[] permutacion){
        int[] hijo = hijoBase.clone();
        int permutacionIndex = 0;

        for(int i = 0; i < hijo.length; i++){
            if (hijo[i] == -1) {
                hijo[i] = permutacion[permutacionIndex];
                permutacionIndex++;
            }
        }

        return hijo;
    }
}
