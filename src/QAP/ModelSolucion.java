package QAP;

import QAP.AlgoritmosGeneticos.Cromosoma;

public class ModelSolucion {
    private long timeElapsed;
    private Cromosoma cromosoma;

    public ModelSolucion(long timeElapsed, Cromosoma cromosoma){
        this.timeElapsed = timeElapsed;
        this.cromosoma = cromosoma;
    }

    public long getTimeElapsed() {
        return timeElapsed;
    }

    public int getCoste(){
        return cromosoma.getCoste();
    }

    @Override
    public String toString() {
        return (cromosoma.toString() + "\n") + "Coste: " + getCoste() + "\n" + "Tiempo empleado: " + timeElapsed + "\n";
    }
}
